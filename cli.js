#!/usr/bin/env node
const rangePattern = /(\d+)(?:(?:\.\.\.?|\-)(\d+))?|all/
const forbiddenChars = /[/<>:"\\|?*\x00-\x1F]/g
const trailingFileJumbo = / *(?:WEBDL|Bluray|HDTV).*(\.\w+)$/
const https = require('https')
const { resolve } = require('path')
const ProgressBar = require('progress')
const request = require('superagent')
const fs = require('fs')
const argv = require('yargs')
  .usage('Usage: $0 <command>')
  .demandCommand(1)
  .command('download <media id>', 'Downloads media from a plex server', y => {
    y.option('s', {
      describe: 'Season(s) to download',
      alias: 'season',
      default: 'all',
      coerce: s => {
        const match = `${s}`.match(rangePattern)
        const range = [ +match[1] || 1, +match[2] || +match[1] ]
        if (s === 'all') range[1] = 0
        if (range[1] < range[0] && range[1]) range[1] = range[0]
        return range
      }
    })
    .option('e', {
      describe: 'Episode(s) to download',
      alias: 'episode',
      default: 'all',
      coerce: e => {
        const match = `${e}`.match(rangePattern)
        const range = [ +match[1] || 1, +match[2] || +match[1] ]
        if (e === 'all') range[1] = 0
        if (range[1] < range[0] && range[1]) range[1] = range[0]
        return range
      }
    })
    .option('o', {
      describe: 'Directory to output the downloaded files to',
      default: '.',
      coerce: resolve
    })
    .check(argv => {
      if (!rangePattern.test(`${argv.s}`))
        throw new Error('Season must be a valid number or range')
      if (!rangePattern.test(argv.e))
        throw new Error('Episode must be a valid number or range')
      return true
    })
  })
  // .command('config <property> [value]', 'a', y => {
  //   y.op
  // })
  .describe('server', 'Server to connect to')
  .describe('token', 'Server token used for authorization')
  .help()
  .completion()
  // .getCompletion()
  .argv

console.log(argv)
// process.exit()

const token = argv.token || process.env.PLEX_TOKEN
const server = argv.server || process.env.PLEX_SERVER

function softMkdir (dir) {
  if (!fs.existsSync(dir))
    fs.mkdirSync(dir)
}
function getHQPart (e) {
  const { key, file } = e.Media[0].Part[0]
  return {
    path: key,
    filename: file.split('/').slice(-1)[0].replace(trailingFileJumbo, '$1')
  }
}
function downloadFile (template, path, filepath) {
  const [hostname, port] = server.split(':')
  return new Promise(resolve => {
    const req = https.request({
      host: hostname,
      port: +port || 443,
      path
    })

    req.on('response', res => {
      if (fs.existsSync(`${filepath}.tmp`)) fs.writeFileSync(`${filepath}.tmp`, '')
      const size = parseInt(res.headers['content-length'], 10)
      let counter = 0

      console.log()
      const bar = new ProgressBar(template, {
        complete: '=',
        incomplete: ' ',
        width: 25,
        total: size
      })

      const refresh = setInterval(() => {
        if (counter > 0) {
          bar.tick(counter)
          counter = 0
        }
      }, 20)

      res.on('data', chunk => {
        fs.appendFileSync(`${filepath}.tmp`, chunk)
        counter += chunk.length
      })

      res.on('end', () => {
        clearInterval(refresh)
        if (fs.existsSync(filepath)) fs.unlinkSync(filepath)
        fs.renameSync(`${filepath}.tmp`, filepath)
        resolve()
      })
    })

    req.end()
  })
}
async function main () {
  const mediaResponse = await request.get(`https://${server}/library/metadata/${argv.mediaid}?X-Plex-Token=${token}`)
    .set('Accept', 'application/json')
    .catch(error => {
      switch (error.status) {
        case 404: {
          throw 'Error: Cannot find media'
        }
        default: {
          throw error
        }
      }
    })
  const media = mediaResponse.body.MediaContainer.Metadata[0]
  const mediaTitle = media.title
  const mediaDir = mediaTitle.replace(forbiddenChars, '')
  const seasonCount = media.childCount
  switch (media.type) {
    case 'show': {
      console.log(`Found ${seasonCount} seasons of ${mediaTitle}`)
      if (argv.s[1] > seasonCount)
        return console.error('Error: Season requested not found')
      const seasonsResponse = await request.get(`https://${server}/library/metadata/${argv.mediaid}/children?X-Plex-Token=${token}`)
        .set('Accept', 'application/json')
      const seasons = seasonsResponse.body.MediaContainer.Metadata.map(s => {
        return {
          title: s.title,
          key: s.ratingKey
        }
      })
      for (let i = argv.s[0]; i <= (argv.s[1] || seasons.length); i++) {
        const season = seasons[i - 1]
        console.log(`Getting data for ${season.title}`)
        const episodesResponse = await request.get(`https://${server}/library/metadata/${season.key}/children?X-Plex-Token=${token}`)
          .set('Accept', 'application/json')
        season.episodes = episodesResponse.body.MediaContainer.Metadata.map(getHQPart)
      }
      softMkdir(argv.o)
      softMkdir(resolve(argv.o, mediaDir))
      for (let i = argv.s[0]; i <= (argv.s[1] || seasons.length); i++) {
        const season = seasons[i - 1]
        let m = 1
        let n = season.episodes.length
        if (argv.s[0] === argv.s[1]) {
          m = argv.e[0]
          n = argv.e[1] || n
          if (n > season.episodes.length)
            return console.error('Error: Episode requested not found')
        }
        softMkdir(resolve(argv.o, mediaDir, season.title))
        for (let j = m; j <= n; j++) {
          const episode = season.episodes[j - 1]
          const episodeId = `S${i.toString().padStart(2, 0)}E${j.toString().padStart(2, 0)}`
          await downloadFile(`  Downloading ${episodeId}     :percent [:bar]   :totalb    :ratebps    in :etas`, episode.path + `?X-Plex-Token=${token}`, resolve(argv.o, mediaTitle, season.title, episode.filename))
        }
      }
      break
    }
    case 'movie': {
      const a = ''
      softMkdir(argv.o)
      softMkdir(resolve(argv.o, mediaDir))
      const movie = getHQPart(media)
      await downloadFile(`  Downloading ${mediaTitle}    :percent [:bar]   :totalb    :ratebps    in :etas`, movie.path + `?X-Plex-Token=${token}`, resolve(argv.o, mediaDir, movie.filename))
      break
    }
    default: {
      console.log(`Error: Unsupported media type "${media.type}"`)
    }
  }
  console.log()
}

main().catch(console.error)
